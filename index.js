var express = require('express');
var path = require('path');
var app = express();

// Define the port to run on
app.set('port', (process.env.PORT || 1778));

app.use(express.static(path.join(__dirname, 'public')));

// Listen for requests

app.listen(1778, '0.0.0.0', function() {
  console.log("... port %d in %s 1778 mode");
});
